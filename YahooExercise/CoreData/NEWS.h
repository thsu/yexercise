//
//  NEWS.h
//  YahooExercise
//
//  Created by Toby Hsu on 2015/9/1.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface NEWS : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSString * itemID;
@property (nonatomic, retain) NSData * image;

@end
