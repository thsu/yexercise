//
//  NEWS.m
//  YahooExercise
//
//  Created by Toby Hsu on 2015/9/1.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

#import "NEWS.h"


@implementation NEWS

@dynamic title;
@dynamic content;
@dynamic itemID;
@dynamic image;

@end
