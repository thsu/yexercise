//
//  NewsViewController.h
//  YahooExercise
//
//  Created by Toby Hsu on 2015/8/31.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsViewController : UITableViewController

@property (strong, nonatomic) NSMutableArray *items;
@property (strong, nonatomic) NSMutableArray *moreItems;
@property (assign) int itemsEndIndex;
@property (assign) int moreItemsIndex;

- (void)refreshView: (UIRefreshControl *) refresh;

@end
