//
//  DetailViewController.m
//  YahooExercise
//
//  Created by Toby Hsu on 2015/8/31.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@property (strong, nonatomic) NSDictionary *content;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isReachable"]) {
        NSLog(@"getItemContent");
        [self getItemContent];
    } else {
        [self getTempContent];
        NSLog(@"getTempContent");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Function

- (void)getTempContent {
    NSString *contentHTML = [[NSString alloc] initWithFormat:@"<html><body>%@</body></html>", self.item[@"content"]];
    [self.webView loadHTMLString:contentHTML baseURL:nil];
    self.title = self.item[@"title"];
}

- (void)getItemContent {
    AFHTTPRequestOperation *operation = [[APIUtil sharedInstance] getServerResponseWithURL:
                                         [NSString stringWithFormat:@"newsitems/%@/content?format=json", self.item[@"id"]]];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation * operation, id responseObject) {
        NSDictionary *dict = (NSDictionary*) responseObject;
        NSDictionary *result = dict[@"result"];
        self.content = result[@"content"];
        NSString *contentHTML = [[NSString alloc] initWithFormat:@"<html><body>%@</body></html>", self.content[@"content"]];
        [self.webView loadHTMLString:contentHTML baseURL:nil];
        self.title = self.content[@"title"];
    } failure:^(AFHTTPRequestOperation * operation , NSError * error) {
        NSLog(@"%@", [error localizedDescription]);
    }];
    [operation start];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
