//
//  NewsViewController.m
//  YahooExercise
//
//  Created by Toby Hsu on 2015/8/31.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

#import "NewsViewController.h"
#import "DetailViewController.h"
#import <CoreData/CoreData.h>

#define refreshCount ((int) 10)
@interface NewsViewController () {
    __block int dataCount;
    __block int minDataCount;
}

@end

@implementation NewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isReachable"]) {
        NSLog(@"getServerData");
        [self getServerData];
    } else {
        [self getLocalData];
        NSLog(@"getLocalData");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Function 

- (void)setup {
    self.items = [[NSMutableArray alloc] init];
    self.moreItems = [[NSMutableArray alloc] init];
    self.moreItemsIndex = 0;
    
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to refresh" ];
    [refresh addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refresh;
}

- (void)getLocalData {
    NSArray *newsArray = [[CoreDataUtil sharedInstance] fetchEntityWithName:@"NEWS"];
    for (NEWS *news in newsArray) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        if (news.title) {
            [dict setObject:news.title forKey:@"title"];
        }
        if (news.image) {
            [dict setObject:news.image forKey:@"image"];
        }
        if (news.content) {
            [dict setObject:news.content forKey:@"content"];
        }
        if (news.itemID) {
            [dict setObject:news.itemID forKey:@"id"];
        }
        [self.items addObject:dict];
    }
    NSLog(@"items count: %lu", (unsigned long)self.items.count);
}

- (void)getServerData {
    [[CoreDataUtil sharedInstance] clearEntityWithName:@"NEWS"];
    AFHTTPRequestOperation *operation = [[APIUtil sharedInstance] getServerResponseWithURL:@"newsfeed?format=json"];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation * operation, id responseObject) {
        NSDictionary *dict = (NSDictionary*) responseObject;
        NSDictionary *result = dict[@"result"];
        dataCount = (int)([result[@"items"] count] + [result[@"more_items"] count]);
        minDataCount = dataCount - (refreshCount * 3); // at least 30 more_items
        for (NSDictionary *item in result[@"items"]) {
            [self getItemWithID:item[@"id"] array:self.items];
        }
        for (NSDictionary *item in result[@"more_items"]) {
            [self getItemWithID:item[@"id"] array:self.moreItems];
        }
    } failure:^(AFHTTPRequestOperation * operation , NSError * error) {
        NSLog(@"%@", [error localizedDescription]);
    }];
    [operation start];
}

- (void)getItemWithID:(NSString*) itemId array:(NSMutableArray*)array {
    AFHTTPRequestOperation *operation = [[APIUtil sharedInstance] getServerResponseWithURL:[NSString stringWithFormat:@"newsitems?format=json&uuid=%@", itemId]];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_queue_t queue = dispatch_queue_create("coreDataQueue", NULL);
        dispatch_async(queue, ^(void) {
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation * operation, id responseObject) {
                NSDictionary *dict = (NSDictionary*) responseObject;
                NSDictionary *result = dict[@"result"];
                if ([result[@"items"] count] > 0) {
                    NSDictionary *dict = result[@"items"][0];
                    [array addObject:dict];
                    
                    
                    NSManagedObject *item = [NSEntityDescription insertNewObjectForEntityForName:@"NEWS" inManagedObjectContext:[CoreDataUtil sharedInstance].managedObjectContext];
                    [item setValue:dict[@"id"] forKey:@"itemID"];
                    if (dict[@"content"]) {
                        [item setValue:dict[@"content"] forKey:@"content"];
                    } else {
                        [item setValue:@"" forKey:@"content"];
                    }
                    [item setValue:dict[@"title"] forKey:@"title"];
                    NSString *urlString = [self getSquareImageURL:dict];
                    if (urlString) {
                        NSURL *url = [[NSURL alloc] initWithString:urlString];
                        NSData *imageData = [NSData dataWithContentsOfURL:url];
                        [item setValue:imageData forKey:@"image"];
                    } else {
                        [item setValue:[NSData new] forKey:@"image"];
                    }
                    [[CoreDataUtil sharedInstance] save];
                    
                    
                }
                if (--dataCount == minDataCount) { // get all response data then reloadData
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                }
            } failure:^(AFHTTPRequestOperation * operation , NSError * error) {
                NSLog(@"%@", [error localizedDescription]);
            }];
        });
    });
    [operation start];
}

- (NSString *)getSquareImageURL:(NSDictionary*)item {
    if (item[@"images"]) {
        for (NSDictionary *dict in item[@"images"]) {
            if ([dict[@"tags"][0] isEqualToString:@"ios:size=square_large"]) {
                return dict[@"url"];
            }
        }
    }
    return nil;
}

- (void)refreshView:(UIRefreshControl *) refresh{
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing Data"];
    double delayInSeconds = 1.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        int pageCount = (int)self.moreItems.count - self.moreItemsIndex;    // MAX_moreItems = 144
        pageCount = pageCount > refreshCount ? refreshCount : pageCount;
        NSLog(@"pageCount: %d", pageCount);
        for (int i=self.moreItemsIndex; i<(self.moreItemsIndex + pageCount); i++) {
            [self.items addObject:self.moreItems[i]];
        }
        if (self.moreItemsIndex == self.moreItems.count) {  // no moreItems, reset items
            [self.items removeObjectsInArray:self.moreItems];
            self.moreItemsIndex = 0;
        }
        else {
            self.moreItemsIndex += pageCount;
        }
        [self.tableView reloadData];
        [refresh endRefreshing];
        
    });
    
}

#pragma mark - TableView DataSource & Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    NSDictionary *item = self.items[indexPath.row];
    UIImage *image = [UIImage imageNamed:@"no_image"];
    [cell.imageView setImage: image];
    cell.textLabel.text = item[@"title"];
    cell.tag = indexPath.row;
    if ([item[@"image"] isKindOfClass:[NSData class]]) {
        [cell.imageView setImage:[UIImage imageWithData:item[@"image"]]];
    } else {
        NSString *url = [self getSquareImageURL:item];
        if (url) {
            NSURL *imageUrl = [[NSURL alloc] initWithString:url];
            [cell.imageView sd_setImageWithURL:imageUrl placeholderImage:image options:SDWebImageRefreshCached];
        }
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ToDetail"]) {
        DetailViewController *destVC = (DetailViewController*)segue.destinationViewController;
        destVC.item = self.items[[sender tag]];
    }
    
}

@end
