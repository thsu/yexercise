//
//  APIUtil.h
//  YahooExercise
//
//  Created by Toby Hsu on 2015/8/31.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIUtil : NSObject

+ (instancetype)sharedInstance;
- (AFHTTPRequestOperation*)getServerResponseWithURL:(NSString*)urlString;

@end
