//
//  CoreDataUtil.m
//  YahooExercise
//
//  Created by Toby Hsu on 2015/9/1.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

#import "CoreDataUtil.h"
#import "AppDelegate.h"

@implementation CoreDataUtil

+ (instancetype)sharedInstance {
    static id sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (NSManagedObjectContext*)managedObjectContext {
    static NSManagedObjectContext *managedObjectContext = nil;
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    if (delegate.managedObjectContext) {
        managedObjectContext = delegate.managedObjectContext;
    }
    return managedObjectContext;
}

- (NSArray *)fetchEntityWithName:(NSString *)name {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:name];
    NSError *error;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects != nil) {
        return fetchedObjects;
    }
    return nil;
}

- (void)save {
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Problem saving: %@", [error localizedDescription]);
    }
}

- (void)clearEntityWithName:(NSString *)name {
    NSArray *allObjects = [self fetchEntityWithName:name];
    for (NSManagedObject *object in allObjects) {
        [self.managedObjectContext deleteObject:object];
    }
    [self save];
}

@end
