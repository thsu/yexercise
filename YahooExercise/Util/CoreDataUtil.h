//
//  CoreDataUtil.h
//  YahooExercise
//
//  Created by Toby Hsu on 2015/9/1.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface CoreDataUtil : NSObject

+ (instancetype)sharedInstance;
- (NSManagedObjectContext*)managedObjectContext;
- (NSArray *)fetchEntityWithName:(NSString *)name;
- (void)save;
- (void)clearEntityWithName:(NSString *)name;

@end
