//
//  APIUtil.m
//  YahooExercise
//
//  Created by Toby Hsu on 2015/8/31.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

#import "APIUtil.h"

@implementation APIUtil

static NSString *baseURL = @"http://mhr.yql.yahoo.com/v1/";

+ (instancetype)sharedInstance {
    static id sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (AFHTTPRequestOperation*)getServerResponseWithURL:(NSString*)urlString {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", baseURL, urlString]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    operation.responseSerializer.acceptableContentTypes = [operation.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
    return operation;
}

@end
